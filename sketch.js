// Phyllotaxis, basically r = c * sqrt(n)

var n = 0;
var c = 3;
var points = [];
var ran;
var start = 0;

function setup() {
  createCanvas(800, 800);
  angleMode(DEGREES);
  colorMode(HSB);
	ran = random(0,255);
}

function draw() {
  
  translate(width / 2, height / 2);
  rotate(n * 0.2);
  
  var a = n * 137.5;
  var r = c * sqrt(n);
  var x = r * cos(a);
  var y = r * sin(a);    
		  
  fill(n%255, n, n);
  noStroke();
  ellipse(x, y, 4, 4);
  
  n += 5;
  
}